const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const tasks = require('./routers/tasks');
const users = require('./routers/users');

const app = express();

app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/users', users);
app.use('/tasks', tasks);

const run = async () => {
    await mongoose.connect('mongodb://localhost/todolist');

    app.listen(port, () => {
        console.log('Server works on port: ', port);
    });

    exitHook(() => {
        console.log('exiting');
        mongoose.disconnect();
    });
};

run().catch(e => console.log(e));