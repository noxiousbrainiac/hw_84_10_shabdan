const mongoose = require('mongoose');

const TodoSchema = new mongoose.Schema({
   user_id: {
       type: mongoose.Types.ObjectId,
       ref: 'User',
       required: true,
       immutable: true
   },
    title: {
       type: String,
       required: true
    },
    description: String,
    status: {
       type: String,
       enum: ['new', 'in_progress', 'complete'],
       required: true
    }
});

const ToDo = mongoose.model('ToDo', TodoSchema);

module.exports = ToDo;