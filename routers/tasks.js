const express = require('express');
const ToDo = require('../models/ToDo');
const auth = require('../middleware/authorization');

const router = express.Router();

router.post('/', auth, async (req, res) => {
    try {
        if (!req.body.user_id || !req.body.title || !req.body.status) {
            return res.status(400).send({error: 'Invalid data'});
        }

        const taskData = {
            user_id: req.body.user_id,
            title: req.body.title,
            description: req.body.description,
            status: req.body.status
        }

        const task = new ToDo(taskData);
        await task.save();
        res.send(task);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', auth, async (req, res) => {
    try {
        const tasks = await ToDo.find();
        res.send(tasks);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.put('/:id', auth, async (req, res) => {
    try {
    const task = await ToDo.findById(req.params.id);
    if(task) {
        if(task.user.equals(req.user._id) {
            for (const key in req.body) {
                task[key] = key !== 'user' ? req.body[key] : task[key];
            }
            await task.save();
            return res.send(`Task ${task.title} was updated by ${req.user.username}`);
        } else {
            return res.status(403).send({error: 'Вы не имеете прав на редактирование этой задачи.'});
        }
    } else {
        return res.status(404).send({error: `Task with ID: ${req.params.id} not found`});
    }
    
        
        if (!req.body.user_id || !req.body.title || !req.body.status) {
            return res.status(400).send({error: 'Invalid data'});
        }

        const taskData = {
            user_id: req.body.user_id,
            title: req.body.title,
            description: req.body.description,
            status: req.body.status
        }

        await ToDo.findByIdAndUpdate(req.params.id, taskData);

        res.send(taskData);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
   try {
       const task = await ToDo.findByIdAndRemove(req.params.id);
       if (req.user.id.toString() !== task.user_id) {
           return res.status(401).send({error: "Not authorized"});
       }
       
       if (task) {
           return res.send(`Task ${task.title} removed`);
        } else {
           return res.status(404).send({error: 'Task not found'});
        }
   } catch (e) {
       res.send(500).send(e);
   }
});

module.exports = router;